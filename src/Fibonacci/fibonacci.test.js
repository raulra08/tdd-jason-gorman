const Fibonacci = require("./fibonacci")

describe("Fibonacci", () => {
    it.each`
        position
        ${0}
        ${1}
    `("should return $position for position = $position", ({position}) => {
        expect(position).toBe(Fibonacci.fibonacciAt(position))
    });

    it.each`
        position | result
        ${2}     | ${1}
        ${3}     | ${2}
        ${5}     | ${5}
    `("should return the sum of the fibonacci numbers of the two preceding numbers ($result) for position = $position",
        ({position, result}) => {
            expect(result).toBe(Fibonacci.fibonacciAt(position))
        });
});