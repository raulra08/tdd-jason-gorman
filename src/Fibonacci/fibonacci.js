const fibonacciAt = position => {
    if (position < 2) {
        return position;
    } else {
        return fibonacciAt(position - 2) + fibonacciAt(position - 1);
    }
};

module.exports = {fibonacciAt}